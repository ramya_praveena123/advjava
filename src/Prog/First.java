package Prog;

public class First {

	public static int trap(int[] height) {
	    int left = 0;
	    int right = height.length - 1;
	    int leftMax = height[left]; // Initialize to the first element
	    int rightMax = height[right]; // Initialize to the last element
	    int result = 0;

	    while (left < right) {
	        leftMax = Math.max(leftMax, height[left]);
	        rightMax = Math.max(rightMax, height[right]);

	        if (height[left] < height[right]) {
	            result += leftMax - height[left];
	            left++;
	        } else {
	            result += rightMax - height[right];
	            right--;
	        }
	    }

	    return result;
	}

	public static void main(String[] args) {
	    int[] height1 = {0, 1, 0, 2, 1, 0, 1, 3, 2, 1, 2, 1};
	    System.out.println(trap(height1));  // Output: 6

	    int[] height2 = {4, 2, 0, 3, 2, 5};
	    System.out.println(trap(height2));  // Output: 9
	}
}